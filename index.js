/**********
* This file contains all function that
* supports isOpenNow function and
* getAvailableTimeSlot function
*
* isOpenNow(open_hours, time_zone)
* checks if the store is open in a specific time zone
*
* getTempTime = (time, minute)
* gets the every time slot based on the range and interval
*
* addMinute = (openTime, closeTime, intervalMinute, rangeMinute)
* adds the range to each time interval
*
* getAvailableTimeSlot = (open_time time_zone, range = 30, interval = 15, days = 3)
* return all availeble delivery time slot based on the open_hours info
*
***********/

const time = {};

//Create day array to match each day property in the open_hours
let day = ["SUN", "MON", "TUE", "WED", "THU", "FRI", "SAT"];
let date = new Date();
let dateName = day[date.getDay()];
let openInfo;
let scheduleTime, openTime, closeTime;
let currentHour, currentMinute;
let availableDeliveryTime = [];
let scheduleTimeBreak, openTimeBreak, closeTimeBreak;

/**
* This function checks if the store is open in a specific time zone
* @param open_hours, JSON data of store's open hours
* @param time_zone, the time zone input to check if store is open in the given time time zone
* @return, Boolean, true if the store is open, false if store is closed
**/
time.isOpenNow = (open_hours, time_zone) => {
  time_zone = (typeof time_zone !== 'undefined') ? time_zone : console.log("Please enter time_zone parameter");

  date = new Date().toLocaleString("en-US", {
    timeZone: time_zone
  });
  //Get date info and open time based on time zone
  date = new Date(date);
  this.open_hours = JSON.parse(open_hours);

  openInfo = this.open_hours[dateName];
  //Get cureent time info
  currentHour = date.getHours();
  currentMinute = date.getMinutes();
  //Split out the open time and close time info based on the open_hours data
  if (openInfo !== "CLOSED" && openInfo != '-1' && openInfo !== "ALL" && openInfo != "1") {
    scheduleTime = openInfo.split('-');
    openTime = scheduleTime[0].split(':');
    closeTime = scheduleTime[scheduleTime.length - 1].split(':');
  } else if (openInfo === "ALL" || openInfo == "1") {
    console.log("The store is open 24/7 at any time zone");
    return true;
  } else {
    console.log("The store is closed every day");
    return false;
  }
  //Compare the current hour and minute
  // with the sorted data about open time and close time
  // to check if the store is open or closed
  if (scheduleTime.length < 3) {
    if (currentHour >= parseInt(openTime[0]) && currentMinute >= parseInt(openTime[1]) && currentHour < parseInt(closeTime[0])) {
      console.log("The store is open at this time zone, the open hour is " + this.open_hours[dateName]);
      return true;
    } else if (currentHour === parseInt(closeTime[0]) && currentMinute < parseInt(closeTime[1])) {
      console.log("The store is open at this time zone, the open hour is " + this.open_hours[dateName]);
      return true;
    } else {
      console.log("The store is closed at this time zone");
      return false;
    }
  }

  if (scheduleTime.length >= 3) {
    scheduleTimeBreak = scheduleTime[1].split(',');
    openTimeBreak = scheduleTimeBreak[1].split(':');
    closeTimeBreak = scheduleTimeBreak[0].split(':');
    let dateNamePre;
    let openInfoPre;
    let scheduleTimePre;
    if (currentHour >= parseInt(openTime[0]) && currentHour < parseInt(closeTimeBreak[0])) {
      console.log("The store is open at this time zone, the open hour is " + this.open_hours[dateName]);
      return true;
    } else if (currentHour >= parseInt(openTimeBreak[0])) {
      if (closeTime[0] > 0 && closeTime[0] < 10) {
        console.log("The store is open at this time zone, the open hour is " + this.open_hours[dateName]);
        return true;
      }
      if (currentHour < closeTime[0]) {
        console.log("The store is open at this time zone, the open hour is " + this.open_hours[dateName]);
        return true;
      }
    } else if (currentHour === parseInt(closeTimeBreak[0]) && currentMinute < parseInt(closeTimeBreak[1])) {
      console.log("The store is open at this time zone, the open hour is " + this.open_hours[dateName]);
      return true;
    } else if (currentHour === parseInt(closeTime[0]) && currentMinute < parseInt(closeTime[1])) {
      console.log("The store is open at this time zone, the open hour is " + this.open_hours[dateName]);
      return true;
    } else if (currentHour >= 0 && currentHour < 6)  // Check the cross day open_hours by compare the current time with previous day's open_hours data
      date.getDay() !== 0 ? dateNamePre = day[date.getDay() - 1] : dateNamePre = day[6];
      openInfoPre = this.open_hours[dateName];
      if (openInfoPre !== "CLOSED" && openInfoPre != '-1' && openInfoPre !== "ALL" || openInfoPre != "1") {
        scheduleTimePre = openInfoPre.split('-');
        if (scheduleTimePre.length >= 3) {
          let closeTimePre = scheduleTimePre[scheduleTimePre.length - 1].split(':');
          if (currentHour < parseInt(closeTimePre[0])) {
            console.log("The store is open at this time zone, the open hour is " + this.open_hours[dateName]);
            return true;
          } else if (currentHour === parseInt(closeTimePre[0] && currentMinute < parseInt(closeTimePre[1]))) {
            console.log("The store is open at this time zone, the open hour is " + this.open_hours[dateName]);
            return true;
          } else {
            console.log("The store is closed at this time zone");
            return false;
          }
        }
      }
    } else {
      console.log("The store is closed at this time zone");
      return false;
    }
  }

/**
 * Add up the time each time by the given time and range or interval
 * This function is used for generating both time interval and time range
 * @param time, the given time value
 * @param minute, the given time interval and time range
 * @return String,  interval time or range time.
 **/
const getTempTime = (time, minute) => {
  date = new Date('July 02, 2019 ' + time);
  date = new Date(date.getTime() + minute * 60000);
  let tempTime = ((date.getHours().toString().length == 1) ? '0' + date.getHours() : date.getHours()) + ':' +
    ((date.getMinutes().toString().length == 1) ? '0' + date.getMinutes() : date.getMinutes()) + ':' +
    ((date.getSeconds().toString().length == 1) ? '0' + date.getSeconds() : date.getSeconds());
  return tempTime;
}
/**
 * Generate the completed time slot info by the given open_hours data
 * Put the result into array
 * @param openTime, open time value
 * @param closeTime, close time value
 * @param intervalMinute, time interval value
 * @param rangeMinute, time range value
 **/
const addMinute = (openTime, closeTime, intervalMinute, rangeMinute) => {
  let startTime = openTime;
  let intervalTime;
  let rangeTime;
  let period;

  while (rangeTime != closeTime) {
    intervalTime = getTempTime(startTime, intervalMinute);
    rangeTime = getTempTime(startTime, rangeMinute);
    period = startTime + " - " + rangeTime;
    startTime = intervalTime;
    availableDeliveryTime.push(period);
  }
}

/**
* This function generate all availeble delivery time slot
* based on the open_hours info in the json format
* @param time_zone, the time zone input to check if store
* is open in the given time time zone
* @param range, the time range to dermine the delivery time range,
* default 30 mins
* @param interval, the time interval to determine the delivery time interval
* defualt 15 mins
* @param days, check how many days of available delivery date user want to print out
* default 3 days
**/
time.getAvailableTimeSlot = (open_hours, time_zone, range = 30, interval = 15, days = 3) => {

  time_zone = (typeof time_zone !== 'undefined') ? time_zone : console.log("Please enter time_zone parameter");

  let dateTime = new Date().toLocaleString("en-US", {
    timeZone: time_zone
  });

  //Set cureent date, open and close time info
  dateTime = new Date(date);
  let specificDate;
  let open_time, close_time, close_break, open_break;
  let deliveryTime = [];
  let allDeliveryDays = {};

  this.open_hours = JSON.parse(open_hours);
  //Generate delivery time based on given days
  if (time.isOpenNow(open_hours, time_zone)) {
    let count = 0;
    dateTime.setDate(dateTime.getDate());
    while (count != days) {
      specificDate = dateTime.getFullYear() + "-" + ((dateTime.getMonth().toString().length == 1) ? "0" + (dateTime.getMonth() + 1) : (dateTime.getMonth() + 1)) + "-" + ((dateTime.getDay().toString().length == 1) ? "0" + (dateTime.getDate()) : (dateTime.getDate()));
      dateName = day[dateTime.getDay()];
      openInfo = this.open_hours[dateName];
      scheduleTime = openInfo.split('-');
      openTime = scheduleTime[0].split(':');
      closeTime = scheduleTime[scheduleTime.length - 1].split(':');

      //Check if the store open cross day on the given day
      //Set up open and close time for generating time slot
      if (scheduleTime.length < 3) {
        if (scheduleTime[0] == "ALL" || scheduleTime[0] == "1") {
          open_time = "00:00:00";
          close_time = "00:00:00";
          //call addMinute to crete time slot
          addMinute(open_time, close_time, interval, range);
        } else if(scheduleTime[0] == "CLOSED") {
          availableDeliveryTime.push("The store is closed today, no delivery available.");
        } else {
          open_time = openTime[0] + ":" + openTime[1] + ":00";
          close_time = closeTime[0] + ":" + closeTime[1] + ":00";
          //call addMinute to crete time slot
          addMinute(open_time, close_time, interval, range);
        }
        //Put the reuslt into array then put it into the object
        deliveryTime = [...availableDeliveryTime]
        allDeliveryDays[specificDate] = deliveryTime;
      }

      //Generate open time and close time value for cross day
      if (scheduleTime.length >= 3) {
        scheduleTimeBreak = scheduleTime[1].split(',');
        openTimeBreak = scheduleTimeBreak[1].split(':');
        closeTimeBreak = scheduleTimeBreak[0].split(':');
        open_time = openTime[0] + ":" + openTime[1] + ":00";
        close_time = closeTime[0] + ":" + closeTime[1] + ":00";
        open_break = openTimeBreak[0] + ":" + openTimeBreak[1] + ":00";
        close_break = closeTimeBreak[0] + ":" + closeTimeBreak[1] + ":00";

        //In cross day schedule case, the available time should be seperated in two parts
        addMinute(open_time, close_break, interval, range);
        addMinute(open_break, close_time, interval, range);
        //Put the reuslt into array then put it into the object
        deliveryTime = [...availableDeliveryTime]
        allDeliveryDays[specificDate] = deliveryTime;
      }
      //Rest the array for the time slot of next day
      availableDeliveryTime.length = 0;
      // Set future date
      dateTime.setDate(dateTime.getDate() + 1);
      count ++;
    }
    console.log(JSON.stringify(allDeliveryDays));
  } else {
    console.log("Sorry the store is closed at your time zone, no delivery time is available");
  }
}

if (typeof module !== 'undefined' && typeof module.exports !== 'undefined') {
  module.exports = time;
}
