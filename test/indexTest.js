/****
*This unit test inculdes two functions
* First one is to check if the store's open hour is correct with the given time zone
* Second one is to check if the store's close hour is correct with the given time time zone
*
* For the test, you can change the time_zone infomation to get the time that maches close period and oepn period.
*****/
const assert = require('chai').assert;
const expect = require('chai').expect;
const time = require('../index');

describe('Filed test for time slot', function() {

  it('check the store is open', function () {
    expect(time.isOpenNow('{ "MON": "CLOSED", "TUE": "00:00-22:00", "WED": "00:00-17:00,19:00-03:00","THU": "09:00-17:00", "FRI": "09:00-17:00,22:00-03:00", "SAT": "ALL", "SUN": "ALL" }', "America/New_York")).to.be.true;
  });

  it('check the store is closed', function () {
    expect(time.isOpenNow('{ "MON": "CLOSED", "TUE": "00:00-22:00", "WED": "00:00-17:00,19:00-03:00","THU": "09:00-17:00", "FRI": "09:00-17:00,22:00-03:00", "SAT": "ALL", "SUN": "ALL" }', "Europe/London")).to.be.false;
  });

});
